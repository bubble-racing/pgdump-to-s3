set -e # exit on error

# expected env variables
# PGPASSWORD=
# USERNAME=
# HOST=
# PORT=
# DBNAME=
# BUCKET_ENDPOINT_URL=
# BUCKET_REGION=
# BUCKET_NAME=
# BUCKET_ACCESS_KEY=
# BUCKET_SECRET_KEY=
# BACKUP_NAME=

# Configure custom endpoint
aws configure set plugins.endpoint awscli_plugin_endpoint
cat <<EOF >> ~/.aws/config
[default]
region = ${BUCKET_REGION}
s3 =
  endpoint_url = https://${BUCKET_ENDPOINT_URL}
s3api =
  endpoint_url = https://${BUCKET_ENDPOINT_URL}
EOF

# Set up credentials
cat <<EOF >> ~/.aws/credentials
[default]
aws_access_key_id=${BUCKET_ACCESS_KEY}
aws_secret_access_key=${BUCKET_SECRET_KEY}
EOF

# Fetch backup from bucket
aws s3 cp "s3://${BUCKET_NAME}/backups/${BACKUP_NAME}" "${BACKUP_NAME}"

# Run restore, PGPASSWORD will be used from env
pg_restore \
  --clean \
  --if-exists \
  -h $HOST \
  -p $PORT \
  -U $USERNAME \
  -d $DBNAME \
  $BACKUP_NAME