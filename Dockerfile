FROM postgres:14-alpine

RUN apk upgrade && \
    apk add py3-pip && \
    pip3 install awscli \
         awscli-plugin-endpoint

COPY backup-postgres-to-s3.sh /usr/local/bin/backup-postgres-to-s3.sh
COPY restore-postgres-from-s3.sh /usr/local/bin/restore-postgres-from-s3.sh

CMD /usr/local/bin/backup-postgres-to-s3.sh