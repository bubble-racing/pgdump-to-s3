set -e # exit on error

# expected env variables
# PGPASSWORD=
# USERNAME=
# HOST=
# PORT=
# DBNAME=
# BUCKET_ENDPOINT_URL=
# BUCKET_REGION=
# BUCKET_NAME=
# BUCKET_ACCESS_KEY=
# BUCKET_SECRET_KEY=

# calculated variables
BACKUP_NAME="$(date -u +%Y-%m-%d_%H-%M-%S)_UTC.gz"

# Run backup, PGPASSWORD will be used from env
pg_dump --format custom \
        --clean \
        --if-exists \
        --create \
        --quote-all-identifiers \
        -h $HOST \
        -p $PORT \
        -U $USERNAME \
        -f "$BACKUP_NAME" \
        $DBNAME

# Configure custom endpoint
aws configure set plugins.endpoint awscli_plugin_endpoint
cat <<EOF >> ~/.aws/config
[default]
region = ${BUCKET_REGION}
s3 =
  endpoint_url = https://${BUCKET_ENDPOINT_URL}
s3api =
  endpoint_url = https://${BUCKET_ENDPOINT_URL}
EOF

# Set up credentials
cat <<EOF >> ~/.aws/credentials
[default]
aws_access_key_id=${BUCKET_ACCESS_KEY}
aws_secret_access_key=${BUCKET_SECRET_KEY}
EOF

# Upload backup
aws s3 cp "${BACKUP_NAME}" "s3://${BUCKET_NAME}/backups/${BACKUP_NAME}"